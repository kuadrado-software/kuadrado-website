const login_form = document.getElementById("login-form");
login_form.onsubmit = e => {
    e.preventDefault();
    fetch("/admin-auth", {
        method: "POST",
        body: new URLSearchParams(new FormData(e.target))
    }).then(res => {
        if (res.status >= 400) {
            alert(res.statusText)
        } else {
            window.location.assign("/v/admin-panel")
        }
    }).catch(err => {
        alert(err.statusText || "Server error")
    });
}