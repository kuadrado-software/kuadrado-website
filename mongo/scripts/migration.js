db = new Mongo().getDB("kuadradodb");

adminname = _getEnv("MONGO_INITDB_NON_ROOT_USERNAME");
adminpwd = _getEnv("MONGO_INITDB_NON_ROOT_PASSWORD");

db.auth(adminname, adminpwd);

articles = db.getCollection("articles");

articles.find().forEach(art => {
    art.images = art.images.map(img => "/assets/images/" + img);
    articles.save(art);
});