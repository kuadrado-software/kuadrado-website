run:
	docker compose up

run-dev:
	docker compose -f ./dev.docker-compose.yml up

build:
	docker compose up --build

build-dev:
	docker compose -f ./dev.docker-compose.yml up  --build 

reload-api:
	docker compose restart kuadrado_server

test:
	RESOURCES_DIR="./" CONTEXT=testing cargo test -- --test-threads=1

doc:
	cargo doc --no-deps

bash-api:
	docker exec -it kuadrado_server bash

bash-mongo:
	docker exec -it kuadradodb bash

build-website:
	npm run --prefix ./website build-prod

build-website-debug:
	npm run --prefix ./website build

build-admin:
	npm run --prefix ./admin-frontend build

build-admin-debug:
	npm run --prefix ./admin-frontend build debug

logs:
	docker compose logs -f