FROM kuadsoft/rust-openssl:bullseye-slim
WORKDIR /usr/src/kuadrado_server
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
COPY ./src ./src