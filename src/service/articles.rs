use crate::{
    middleware::AuthenticatedAdminMiddleware,
    model::Article,
    static_view::{create_static_view, delete_static_view},
    AppState,
};
use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path},
    HttpRequest, HttpResponse, Responder,
};
use chrono::Utc;
use futures::stream::StreamExt;
use regex::Regex;
use serde::{Deserialize, Serialize};
use wither::{
    bson::{doc, oid::ObjectId, Bson, DateTime},
    mongodb::Collection,
    prelude::Model,
};

#[derive(Deserialize, Serialize)]
pub struct ArticleTitleFormData {
    pub title: String,
}

fn get_collection(app_state: &AppState) -> Collection<Article> {
    app_state.db.collection_with_type::<Article>("articles")
}

fn slugify(s: &String) -> String {
    let s = s.to_lowercase();
    // Delete every character that is not a letter a number or a space
    let re = Regex::new(r"[^a-z\d\s]").unwrap();
    let slug = re.replace_all(&s, "");

    let slug = slug.trim().to_string();
    // Deduplicate whitespaces and replace them by hyhens
    let re = Regex::new(r"[\s]+").unwrap();
    let slug = re.replace_all(&slug, "-");

    slug.to_string()
}

fn create_article_static_view_metadata(app_state: &AppState, art: &mut Article) {
    if art.with_static_view {
        let article_slug = slugify(&art.title);
        art.metadata.slug = Some(article_slug.to_owned());
        art.metadata.view_uri = Some(format!(
            "{}://{}/{}/view/{}/{}/",
            &app_state.env.server_protocol,
            &app_state.env.server_host,
            &art.category,
            &art.locale,
            &article_slug,
        ));

        art.metadata.static_resource_path = Some(
            app_state
                .env
                .public_dir
                .join(&art.category)
                .join("view")
                .join(&art.locale)
                .join(&article_slug),
        );
    }
}

async fn generate_article_view(app_state: &AppState, id: &ObjectId) -> Result<(), String> {
    match Article::find_one(&app_state.db, doc! {"_id":&id}, None).await {
        Ok(art) => {
            if art.is_none() {
                return Err(format!("Article with id {} was not found", id));
            }

            let art = art.unwrap();
            if art.with_static_view {
                return create_static_view(&app_state, &art);
            }
            Ok(())
        }
        Err(e) => {
            return Err(format!(
                "Database error searching for article with id {} : {}",
                id, e
            ))
        }
    }
}

#[post("/post-article")]
pub async fn post_article(
    app_state: Data<AppState>,
    article_data: Json<Article>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let mut article_data = article_data.into_inner();
    article_data.date = Some(DateTime(Utc::now()));

    create_article_static_view_metadata(&app_state, &mut article_data);

    match get_collection(&app_state)
        .insert_one(article_data, None)
        .await
    {
        Ok(res) => {
            match &res.inserted_id {
                Bson::ObjectId(id) => {
                    if let Err(e) = generate_article_view(&app_state, id).await {
                        return HttpResponse::InternalServerError()
                            .body(format!("Error creating article page {:?}", e));
                    }
                }
                _ => {
                    return HttpResponse::InternalServerError().body("Error creating article page");
                }
            }
            HttpResponse::Created().json(res)
        }
        Err(e) => {
            HttpResponse::InternalServerError().body(format!("Error inserting new article {:?}", e))
        }
    }
}

#[put("/update-article/{article_id}")]
pub async fn update_article(
    app_state: Data<AppState>,
    article_data: Json<Article>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    article_id: Path<String>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let article_id = match ObjectId::with_string(&article_id.into_inner()) {
        Ok(id) => id,
        Err(_) => {
            return HttpResponse::BadRequest()
                .body("Failed to convert article_id to ObjectId. String may be malformed")
        }
    };

    let mut article_data = article_data.into_inner();
    article_data.date = Some(DateTime(Utc::now()));

    if article_data.with_static_view {
        if let Err(e) = delete_static_view(
            &app_state,
            &article_data.metadata.static_resource_path,
            &article_data.metadata.view_uri,
        ) {
            return HttpResponse::InternalServerError()
                .body(format!("Error removing previous static view : {}", e));
        };
    }

    create_article_static_view_metadata(&app_state, &mut article_data);

    match get_collection(&app_state)
        .find_one_and_replace(doc! {"_id": &article_id}, article_data, None)
        .await
    {
        Ok(res) => match res {
            Some(art) => {
                if let Err(e) = generate_article_view(&app_state, &art.clone().id.unwrap()).await {
                    return HttpResponse::InternalServerError()
                        .body(format!("Error generating article static view {}", e));
                };
                HttpResponse::Ok().json(art)
            }
            None => HttpResponse::InternalServerError().finish(),
        },
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

#[delete("/delete-article/{article_id}")]
pub async fn delete_article(
    app_state: Data<AppState>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    article_id: Path<String>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let article_id = match ObjectId::with_string(&article_id.into_inner()) {
        Ok(id) => id,
        Err(_) => {
            return HttpResponse::BadRequest()
                .body("Failed to convert article_id to ObjectId. String may be malformed")
        }
    };

    let articles = get_collection(&app_state);

    match articles.find_one(doc! {"_id": &article_id}, None).await {
        Ok(article) => match article {
            Some(art) => {
                if art.with_static_view {
                    if let Err(e) = delete_static_view(
                        &app_state,
                        &art.metadata.static_resource_path,
                        &art.metadata.view_uri,
                    ) {
                        return HttpResponse::InternalServerError()
                            .body(format!("Error removing article static view {}", e));
                    };
                }

                match articles.delete_one(doc! {"_id": &article_id}, None).await {
                    Ok(_) => HttpResponse::Accepted().body("Article was deleted"),
                    Err(e) => HttpResponse::InternalServerError()
                        .body(format!("Error deleting article  {}", e)),
                }
            }
            None => HttpResponse::NotFound().body("Article was not found"),
        },
        Err(e) => HttpResponse::InternalServerError().body(&format!("{:?}", e)),
    }
}

#[get("/articles/{category}/{locale}")]
pub async fn get_articles_by_category(
    app_state: Data<AppState>,
    path: Path<(String, String)>,
) -> impl Responder {
    let (category, locale) = path.into_inner();

    match get_collection(&app_state)
        .find(doc! {"category": category, "locale": locale}, None)
        .await
    {
        Ok(mut cursor) => {
            let mut results: Vec<Article> = Vec::new();

            while let Some(result) = cursor.next().await {
                match result {
                    Ok(article) => {
                        results.push(article);
                    }
                    Err(e) => {
                        return HttpResponse::InternalServerError().body(format!("{:#?}", e));
                    }
                }
            }
            HttpResponse::Ok().json(results)
        }
        Err(e) => HttpResponse::InternalServerError().body(format!("{:#?}", e)),
    }
}

#[get("/article/{article_id}")]
pub async fn get_article(app_state: Data<AppState>, article_id: Path<String>) -> impl Responder {
    let article_id = match ObjectId::with_string(&article_id.into_inner()) {
        Ok(id) => id,
        Err(_) => {
            return HttpResponse::BadRequest()
                .body("Failed to convert article_id to ObjectId. String may be malformed")
        }
    };

    match Article::find_one(&app_state.db, doc! {"_id":&article_id}, None).await {
        Ok(art) => {
            if art.is_none() {
                return HttpResponse::NotFound().body("Article was not found");
            }

            HttpResponse::Ok().json(art)
        }
        Err(e) => HttpResponse::InternalServerError().body(format!("Database error: {:#?}", e)),
    }
}

#[get("/articles")]
pub async fn get_all_articles(app_state: Data<AppState>) -> impl Responder {
    match get_collection(&app_state).find(None, None).await {
        Ok(mut cursor) => {
            let mut results: Vec<Article> = Vec::new();
            while let Some(result) = cursor.next().await {
                match result {
                    Ok(article) => {
                        results.push(article);
                    }
                    Err(e) => {
                        return HttpResponse::InternalServerError().body(format!("{:#?}", e));
                    }
                }
            }
            HttpResponse::Ok().json(results)
        }
        Err(e) => HttpResponse::InternalServerError().body(format!("{:#?}", e)),
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_articles {
    use super::*;
    use crate::middleware::get_auth_cookie;
    use crate::model::{AdminAuthCredentials, Administrator};
    use actix_web::{
        http::{Method, StatusCode},
        test,
        web::Bytes,
        App,
    };
    use std::fs::remove_dir_all;

    async fn insert_test_article(
        app_state: &AppState,
        test_article: Article,
    ) -> Result<(ObjectId, String), String> {
        let title = test_article.title.to_owned();

        match get_collection(&app_state)
            .insert_one(test_article, None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok((id, title)),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_article(
        app_state: &AppState,
        article_id: &ObjectId,
    ) -> Result<i64, String> {
        match get_collection(&app_state)
            .delete_one(doc! {"_id": article_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    fn clear_testing_static_view_dir(app_state: &AppState) -> Result<(), String> {
        let path = app_state.env.public_dir.join("testing/view");
        if path.exists() && path.is_dir() {
            let res = remove_dir_all(path);
            if let Err(e) = res {
                return Err(format!("Error removing testing static views {}", e));
            }
        }
        Ok(())
    }

    async fn get_authenticated_admin(app_state: &AppState) -> Administrator {
        Administrator::authenticated(
            app_state,
            AdminAuthCredentials {
                username: app_state.env.default_admin_username.to_owned(),
                password: app_state.env.default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap()
    }

    #[tokio::test]
    async fn test_post_article() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(post_article),
        )
        .await;

        let article = Article::test_article();

        let admin_user = get_authenticated_admin(&app_state).await;

        let req = test::TestRequest::with_uri("/post-article")
            .method(Method::POST)
            .header("Content-Type", "application/json")
            .header("Accept", "text/html")
            .cookie(get_auth_cookie(
                "kuadrado-admin-auth",
                app_state
                    .encryption
                    .decrypt(&admin_user.auth_token.unwrap())
                    .to_owned(),
            ))
            .set_payload(Bytes::from(serde_json::to_string(&article).unwrap()))
            .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::CREATED);

        let find_inserted = Article::find_one(&app_state.db, doc! {"title": &article.title}, None)
            .await
            .unwrap();

        assert!(find_inserted.is_some());
        let find_inserted = find_inserted.unwrap();
        assert_eq!(find_inserted.title, article.title);

        // Static view tests
        let static_view_path = find_inserted.metadata.static_resource_path;
        assert!(static_view_path.is_some());

        let static_view_path = static_view_path.unwrap();
        assert!(static_view_path.exists());

        let cleared = clear_testing_static_view_dir(&app_state);
        assert!(cleared.is_ok());

        get_collection(&app_state)
            .delete_one(doc! {"title": article.title}, None)
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_post_article_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(post_article),
        )
        .await;

        let article = Article::test_article();

        let req = test::TestRequest::with_uri("/post-article")
            .method(Method::POST)
            .header("Content-Type", "application/json")
            .header("Accept", "text/html")
            .cookie(get_auth_cookie(
                "wrong-cookie",
                app_state.encryption.random_ascii_lc_string(32),
            ))
            .set_payload(Bytes::from(serde_json::to_string(&article).unwrap()))
            .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn test_update_article() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(update_article),
        )
        .await;

        let mut article = Article::test_article();

        let (article_id, _) = insert_test_article(&app_state, article.clone())
            .await
            .unwrap();

        article.title = "changed title".to_string();

        let admin_user = get_authenticated_admin(&app_state).await;

        let req = test::TestRequest::with_uri(
            format!("/update-article/{}", article_id.to_hex()).as_str(),
        )
        .method(Method::PUT)
        .header("Content-Type", "application/json")
        .header("Accept", "text/html")
        .cookie(get_auth_cookie(
            "kuadrado-admin-auth",
            app_state
                .encryption
                .decrypt(&admin_user.auth_token.unwrap())
                .to_owned(),
        ))
        .set_payload(Bytes::from(serde_json::to_string(&article).unwrap()))
        .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::OK);

        let find_inserted = Article::find_one(&app_state.db, doc! {"_id": &article_id}, None)
            .await
            .unwrap();

        assert!(find_inserted.is_some());
        let find_inserted = find_inserted.unwrap();
        assert_eq!(find_inserted.title, "changed title");

        // Static view tests
        let static_view_path = find_inserted.metadata.static_resource_path;
        assert!(static_view_path.is_some());
        let static_view_path = static_view_path.unwrap();
        assert!(static_view_path.exists());

        let cleared = clear_testing_static_view_dir(&app_state);
        assert!(cleared.is_ok());

        let del_count = delete_test_article(&app_state, &article_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_update_article_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(update_article),
        )
        .await;

        let article = Article::test_article();

        let req = test::TestRequest::with_uri(
            format!("/update-article/{}", ObjectId::new().to_hex()).as_str(),
        )
        .method(Method::PUT)
        .header("Content-Type", "application/json")
        .header("Accept", "text/html")
        .cookie(get_auth_cookie(
            "wrong-cookie",
            app_state.encryption.random_ascii_lc_string(32),
        ))
        .set_payload(Bytes::from(serde_json::to_string(&article).unwrap()))
        .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn test_delete_article() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(delete_article),
        )
        .await;

        let article = Article::test_article();
        let (article_id, _) = insert_test_article(&app_state, article.clone())
            .await
            .unwrap();

        let admin_user = get_authenticated_admin(&app_state).await;

        let req = test::TestRequest::with_uri(
            format!("/delete-article/{}", article_id.to_hex()).as_str(),
        )
        .method(Method::DELETE)
        .cookie(get_auth_cookie(
            "kuadrado-admin-auth",
            app_state
                .encryption
                .decrypt(&admin_user.auth_token.unwrap())
                .to_owned(),
        ))
        .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::ACCEPTED);

        let find_inserted = Article::find_one(&app_state.db, doc! {"_id": &article_id}, None)
            .await
            .unwrap();

        assert!(find_inserted.is_none());
    }

    #[tokio::test]
    async fn test_delete_article_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(delete_article),
        )
        .await;

        let article = Article::test_article();

        let req = test::TestRequest::with_uri(
            format!("/delete-article/{}", ObjectId::new().to_hex()).as_str(),
        )
        .method(Method::DELETE)
        .cookie(get_auth_cookie(
            "wrong-cookie",
            app_state.encryption.random_ascii_lc_string(32),
        ))
        .set_payload(Bytes::from(serde_json::to_string(&article).unwrap()))
        .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn test_get_article() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(get_article),
        )
        .await;

        let article = Article::test_article();
        let (article_id, article_title) = insert_test_article(&app_state, article.clone())
            .await
            .unwrap();

        let req = test::TestRequest::with_uri(format!("/article/{}", article_id.to_hex()).as_str())
            .header("Accept", "application/json")
            .method(Method::GET)
            .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::OK);

        let result: Article = test::read_body_json(resp).await;
        assert_eq!(result.title, article_title);

        let del_count = delete_test_article(&app_state, &article_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_get_articles_by_category() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(get_articles_by_category),
        )
        .await;

        let article = Article::test_article();
        let (article_id, article_title) = insert_test_article(&app_state, article.clone())
            .await
            .unwrap();

        let req = test::TestRequest::with_uri("/articles/testing/fr")
            .header("Accept", "application/json")
            .method(Method::GET)
            .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::OK);

        let results: Vec<Article> = test::read_body_json(resp).await;
        let find_inserted = results.iter().find(|&art| art.title == article_title);
        assert!(find_inserted.is_some());

        let del_count = delete_test_article(&app_state, &article_id).await.unwrap();
        assert_eq!(del_count, 1);
    }
}
