use crate::{core::static_files::*, middleware::AuthenticatedAdminMiddleware, AppState};
use actix_multipart::Multipart;
use actix_web::{
    delete, get, post,
    web::{Data, Path},
    HttpRequest, HttpResponse, Responder,
};
use futures::StreamExt;
use std::{
    fs::{remove_file, File},
    io::Write,
};

fn upload_data_from_multipart_field(field: &actix_multipart::Field) -> Result<UploadData, String> {
    match field.content_disposition() {
        Some(content_disposition) => match content_disposition.get_filename() {
            Some(fname) => match file_ext(&fname.to_string()) {
                Ok(ext) => Ok(UploadData {
                    up_type: upload_type_from_file_ext(&ext),
                    filename: fname.to_owned(),
                }),
                Err(msg) => return Err(msg),
            },
            None => Err("Couldn't retrieve file extension".to_string()),
        },
        None => Err("Missing content disposition".to_string()),
    }
}

async fn write_uploaded_file(
    app_state: &AppState,
    field: &mut actix_multipart::Field,
    filename: &String,
    upload_type: UploadType,
) -> Result<String, String> {
    let uploads_dir = get_uploads_dir(StaticFilesIndex::get_public_dir(&app_state.env));
    let sub_dir = dirname_from_type(&upload_type);
    let filepath = create_dir_if_missing(uploads_dir.join(&sub_dir)).join(&filename);

    match File::create(&filepath) {
        Err(e) => Err(format!("Error creating file {:?} : {:?}", filepath, e)),
        Ok(mut f) => {
            // Field in turn is stream of *Bytes* object
            while let Some(chunk) = field.next().await {
                match chunk {
                    Ok(chunk) => {
                        if f.write_all(&chunk).is_err() {
                            remove_file(&filepath).unwrap();
                            return Err("Error writing chunk".to_string());
                        }
                    }
                    Err(e) => {
                        return Err(format!("Error writing file {} : {:?}", filename, e));
                    }
                }
            }

            Ok(filepath.into_os_string().into_string().unwrap())
        }
    }
}

#[post("/post-files")]
pub async fn post_files(
    app_state: Data<AppState>,
    static_files_index: Data<std::sync::Mutex<StaticFilesIndex>>,
    mut payload: Multipart,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let mut uploaded_filepathes = Vec::new();
    let mut files_index = static_files_index.lock().unwrap();

    while let Some(item) = payload.next().await {
        match item {
            Ok(mut field) => {
                let up_data = upload_data_from_multipart_field(&field);

                if let Err(msg) = up_data {
                    return HttpResponse::InternalServerError().body(msg);
                }

                let up_data = up_data.unwrap();

                match write_uploaded_file(
                    &app_state,
                    &mut field,
                    &up_data.filename,
                    up_data.up_type,
                )
                .await
                {
                    Err(msg) => return HttpResponse::InternalServerError().body(msg),
                    Ok(filepath) => {
                        files_index.push_path(std::path::Path::new(&filepath), &app_state.env);
                        uploaded_filepathes.push(filepath);
                    }
                }
            }
            Err(e) => {
                return HttpResponse::InternalServerError().body(format!("FIELD ERR {:?}", e))
            }
        }
    }

    HttpResponse::Ok().json(uploaded_filepathes)
}

#[get("/static-files-index")]
async fn get_static_files_index(
    static_files_index: Data<std::sync::Mutex<StaticFilesIndex>>,
) -> impl Responder {
    HttpResponse::Ok().json(
        static_files_index
            .lock()
            .expect("Couldn't lock files index")
            .get_index(),
    )
}

#[delete("/delete-file/{dir}/{category}/{filename}")]
async fn delete_static_file(
    app_state: Data<AppState>,
    static_files_index: Data<std::sync::Mutex<StaticFilesIndex>>,
    fileinfo: Path<(String, String, String)>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let (dir, cat, fname) = fileinfo.into_inner();
    let fpath = std::path::PathBuf::from(cat).join(fname);
    let fbasedir = {
        let p = std::path::PathBuf::from("assets");
        if dir == "uploads" {
            p.join("uploads")
        } else {
            p
        }
    };

    match remove_file(
        StaticFilesIndex::get_public_dir(&app_state.env)
            .join(&fbasedir)
            .join(&fpath),
    ) {
        Ok(_) => {
            let indexed_url = std::path::PathBuf::from("/").join(&fbasedir).join(&fpath);
            let mut files_index = static_files_index.lock().unwrap();
            files_index.remove_path(indexed_url.to_str().unwrap().to_owned());
            HttpResponse::Accepted().body("File was deleted")
        }
        Err(e) => HttpResponse::InternalServerError().body(format!("Error deleting file {:?}", e)),
    }
}

// EXAMPLE FROM ACTIX REPO (using threadpool)

// use futures::TryStreamExt;
// use std::io::Write;

// pub async fn save_file(mut payload: Multipart) -> impl Responder {
//     println!("SAVE FILE");
//     // iterate over multipart stream
//     while let Some(mut field) = payload.try_next().await.unwrap() {
//         // A multipart/form-data stream has to contain `content_disposition`
//         let content_disposition = field.content_disposition().unwrap();
//         let filename = content_disposition.get_filename().unwrap();
//         let filepath = format!("./tmp/{filename}");

//         // File::create is blocking operation, use threadpool
//         let mut f = actix_web::web::block(|| std::fs::File::create(filepath))
//             .await
//             .unwrap();

//         // Field in turn is stream of *Bytes* object
//         while let Some(chunk) = field.try_next().await.unwrap() {
//             // filesystem operations are blocking, we have to use threadpool
//             f = actix_web::web::block(move || f.write_all(&chunk).map(|_| f))
//                 .await
//                 .unwrap();
//         }
//     }

//     HttpResponse::Ok().body("sucess")
// }

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_static_files {
    use super::*;
    use crate::{
        core::static_files::StaticFilesIndex,
        middleware::get_auth_cookie,
        model::{AdminAuthCredentials, Administrator},
    };
    use actix_web::{
        http::{Method, StatusCode},
        test,
        web::Bytes,
        App,
    };

    fn create_simple_request() -> Bytes {
        Bytes::from(
            "--abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"file\"; filename=\"test.txt\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 4\r\n\r\n\
             test\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"file\"; filename=\"data.txt\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 4\r\n\r\n\
             data\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        )
    }

    fn create_files_index(app_state: &AppState) -> Data<std::sync::Mutex<StaticFilesIndex>> {
        let mut static_files_index = StaticFilesIndex(Vec::new());
        static_files_index.rebuild(&app_state.env);
        Data::new(std::sync::Mutex::new(static_files_index))
    }

    fn clear_testing_static() {
        std::fs::remove_dir_all(std::env::current_dir().unwrap().join("testing_static")).unwrap();
    }

    #[tokio::test]
    async fn post_files_unauthenticated_should_be_unauthorized() {
        let app_state = AppState::for_test().await;

        let static_files_index = create_files_index(&app_state);

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::clone(&static_files_index))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(post_files),
        )
        .await;

        let req = test::TestRequest::with_uri("/post-files")
            .method(Method::POST)
            .header(
                "Content-Type",
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            )
            .cookie(get_auth_cookie(
                "wrong-cookie",
                app_state.encryption.random_ascii_lc_string(32),
            ))
            .set_payload(create_simple_request())
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn test_post_files() {
        let app_state = AppState::for_test().await;

        let admin_user = Administrator::authenticated(
            &app_state,
            AdminAuthCredentials {
                username: app_state.env.default_admin_username.to_owned(),
                password: app_state.env.default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap();

        let static_files_index = create_files_index(&app_state);

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::clone(&static_files_index))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(post_files),
        )
        .await;

        let req = test::TestRequest::with_uri("/post-files")
            .method(Method::POST)
            .header(
                "Content-Type",
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            )
            .cookie(get_auth_cookie(
                "kuadrado-admin-auth",
                app_state
                    .encryption
                    .decrypt(&admin_user.auth_token.unwrap())
                    .to_owned(),
            ))
            .set_payload(create_simple_request())
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        let status = resp.status();

        assert_eq!(status, StatusCode::OK);

        let pathes: Vec<String> = test::read_body_json(resp).await;
        let public_dir = StaticFilesIndex::get_public_dir(&app_state.env);

        let pathes_from_public = pathes
            .iter()
            .map(|p| {
                format!(
                    "/{}",
                    std::path::Path::new(p)
                        .strip_prefix(&public_dir)
                        .unwrap()
                        .to_str()
                        .unwrap()
                )
            })
            .collect::<Vec<String>>();

        let index = static_files_index.lock().unwrap();

        assert_eq!(pathes_from_public, index.0);

        let mut iter_pathes = pathes.iter();
        let f = std::fs::read_to_string(iter_pathes.next().unwrap()).unwrap();
        assert_eq!(f, "test");
        let f = std::fs::read_to_string(iter_pathes.next().unwrap()).unwrap();
        assert_eq!(f, "data");
        clear_testing_static();
    }

    #[tokio::test]
    async fn test_delete_file() {
        let app_state = AppState::for_test().await;

        let admin_user = Administrator::authenticated(
            &app_state,
            AdminAuthCredentials {
                username: app_state.env.default_admin_username.to_owned(),
                password: app_state.env.default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap();

        let static_files_index = create_files_index(&app_state);

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::clone(&static_files_index))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "kuadrado-admin-auth",
                )))
                .service(post_files)
                .service(delete_static_file),
        )
        .await;

        let auth_token = admin_user.auth_token.unwrap();

        let req = test::TestRequest::with_uri("/post-files")
            .method(Method::POST)
            .header(
                "Content-Type",
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            )
            .cookie(get_auth_cookie(
                "kuadrado-admin-auth",
                app_state.encryption.decrypt(&auth_token).to_owned(),
            ))
            .set_payload(create_simple_request())
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        let status = resp.status();

        assert_eq!(status, StatusCode::OK);

        let req = test::TestRequest::with_uri("/delete-file/uploads/docs/test.txt")
            .method(Method::DELETE)
            .cookie(get_auth_cookie(
                "kuadrado-admin-auth",
                app_state.encryption.decrypt(&auth_token).to_owned(),
            ))
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        let status = resp.status();
        assert_eq!(status, StatusCode::ACCEPTED);
        clear_testing_static();
    }
}
