use actix_files::NamedFile;

pub async fn favicon() -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("RESOURCES_DIR").expect("RESOURCES_DIR is not defined"),
    )
    .join("public/standard");

    Ok(NamedFile::open(standard_dir.join("favicon.ico"))?)
}

pub async fn robots() -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("RESOURCES_DIR").expect("RESOURCES_DIR is not defined"),
    )
    .join("public/standard");

    Ok(NamedFile::open(standard_dir.join("robots.txt"))?)
}

pub async fn sitemap() -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("RESOURCES_DIR").expect("RESOURCES_DIR is not defined"),
    )
    .join("public/standard");

    Ok(NamedFile::open(standard_dir.join("sitemap.xml"))?)
}

pub async fn dyn_sitemap() -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("RESOURCES_DIR").expect("RESOURCES_DIR is not defined"),
    )
    .join("public/standard");

    Ok(NamedFile::open(standard_dir.join("dyn_sitemap.xml"))?)
}
