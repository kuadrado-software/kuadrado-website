use std::env;

#[derive(Debug, Clone)]
/// Makes a copy of all required values defined in the system environment variables
pub struct Env {
    pub release_mode: String,
    pub db_username: String,
    pub db_user_pwd: String,
    pub db_name: String,
    pub db_port: String,
    pub server_host: String,
    pub server_protocol: String,
    pub crypt_key: String,
    pub default_admin_username: String,
    pub default_admin_password: String,
    pub public_dir: std::path::PathBuf,
}

static RELEASE_MODES: [&str; 3] = ["debug", "test", "prod"];

pub fn get_release_mode() -> String {
    let default = "prod";

    match env::var("RELEASE_MODE") {
        Ok(s) => {
            if RELEASE_MODES.contains(&s.as_str()) {
                return s;
            } else {
                return default.to_string();
            }
        }
        Err(_) => default.to_string(),
    }
}

pub fn get_log_level() -> String {
    let rel_mode = get_release_mode();
    match rel_mode.as_str() {
        "debug" => String::from("debug"),
        "test" => String::from("debug"),
        _ => String::from("info"),
    }
}

impl Env {
    pub fn new() -> Env {
        Env {
            release_mode: get_release_mode(),
            db_username: env::var("DB_USERNAME").expect("DB_USERNAME is not defined."),
            db_user_pwd: env::var("DB_USER_PASSWORD").expect("DB_USER_PASSWORD is not defined."),
            db_name: env::var("DATABASE_NAME").expect("DATABASE_NAME is not defined."),
            db_port: env::var("DB_PORT").expect("DB_PORT is not defined."),
            server_host: env::var("SERVER_HOST").expect("SERVER_HOST is not defined"),
            server_protocol: env::var("SERVER_PROTOCOL").expect("SERVER_PROTOCOL is not defined"),
            crypt_key: env::var("CRYPT_KEY").expect("CRYPT_KEY is not defined."),
            default_admin_username: env::var("DEFAULT_ADMIN_USERNAME")
                .expect("DEFAULT_ADMIN_USERNAME is not defined"),
            default_admin_password: env::var("DEFAULT_ADMIN_PASSWORD")
                .expect("DEFAULT_ADMIN_PASSWORD is not defined"),
            public_dir: std::path::PathBuf::from(
                env::var("RESOURCES_DIR").expect("RESOURCES_DIR is not defined"),
            )
            .join("public"),
        }
    }

    #[cfg(test)]
    /// Returns an instance with some values adjusted for testing such as email addresses
    pub fn for_test() -> Env {
        Env {
            release_mode: String::from("debug"),
            db_username: env::var("DB_USERNAME").expect("DB_USERNAME is not defined."),
            db_user_pwd: env::var("DB_USER_PASSWORD").expect("DB_USER_PASSWORD is not defined."),
            db_name: env::var("DATABASE_NAME").expect("DATABASE_NAME is not defined."),
            db_port: env::var("DB_PORT").expect("DB_PORT is not defined."),
            server_host: env::var("SERVER_HOST").expect("SERVER_HOST is not defined"),
            server_protocol: env::var("SERVER_PROTOCOL").expect("SERVER_PROTOCOL is not defined"),
            crypt_key: env::var("CRYPT_KEY").expect("CRYPT_KEY is not defined."),
            default_admin_username: env::var("DEFAULT_ADMIN_USERNAME")
                .expect("DEFAULT_ADMIN_USERNAME is not defined"),
            default_admin_password: env::var("DEFAULT_ADMIN_PASSWORD")
                .expect("DEFAULT_ADMIN_PASSWORD is not defined"),
            public_dir: std::path::PathBuf::from(
                env::var("RESOURCES_DIR").expect("RESOURCES_DIR is not defined"),
            )
            .join("public"),
        }
    }
}
