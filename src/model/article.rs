#[cfg(test)]
use chrono::Utc;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use wither::{
    bson::{doc, oid::ObjectId, DateTime},
    prelude::Model,
};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ArticleDetail {
    pub label: String,
    pub value: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ArticleMetadata {
    pub description: String,
    pub view_uri: Option<String>,
    pub static_resource_path: Option<PathBuf>,
    pub slug: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Model, Clone)]
#[model(index(keys = r#"doc!{"title": 1}"#, options = r#"doc!{"unique": true}"#))]
pub struct Article {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    pub id: Option<ObjectId>,
    pub title: String,
    pub subtitle: String,
    pub date: Option<DateTime>,
    pub body: String,
    pub details: Vec<ArticleDetail>,
    pub images: Vec<String>,
    pub category: String,
    pub locale: String,
    pub display_priority_index: i8,
    pub with_static_view: bool,
    pub metadata: ArticleMetadata,
}

impl Article {
    #[cfg(test)]
    pub fn test_article() -> Self {
        Article {
            id: None,
            title: "Test Article".to_string(),
            subtitle: "An article for testing".to_string(),
            date: Some(DateTime(Utc::now())),
            body: "blablabla".to_string(),
            details: vec![
                ArticleDetail {
                    label: "A label".to_string(),
                    value: "A value".to_string(),
                },
                ArticleDetail {
                    label: "Another label".to_string(),
                    value: "Another value".to_string(),
                },
            ],
            images: vec!["an_image.png".to_string()],
            category: "testing".to_string(),
            locale: "fr".to_string(),
            display_priority_index: 1,
            with_static_view: true,
            metadata: ArticleMetadata {
                description: "A test article".to_string(),
                view_uri: None,
                static_resource_path: None,
                slug: None,
            },
        }
    }
}
