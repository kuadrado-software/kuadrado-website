module.exports = {
    images_url: `/assets/images`,
    data_url: `/assets/data`,
    translations_url: "/assets/translations"
};
