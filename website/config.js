module.exports = {
    build: {
        protected_dirs: ["assets", "style", "views", "standard", "view"],
        default_meta_keys: ["title", "description", "image", "open_graph", "json_ld"],
    },
};
