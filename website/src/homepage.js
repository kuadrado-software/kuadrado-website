"use strict";

const { images_url } = require("../constants");
const ThemeCard = require("./home-page-components/theme-card");
const WebPage = require("./lib/web-page");

class HomePage extends WebPage {
    constructor() {
        super({ id: "home-page" });
    }

    render() {
        return {
            tag: "div",
            id: this.id,
            contents: [
                {
                    tag: "div",
                    class: "page-header",
                    contents: [
                        {
                            tag: "div",
                            class: "big-logo page-contents-center",
                            contents: [
                                {
                                    tag: "img",
                                    alt: "logo Kuadrado",
                                    src: `${images_url}/logo_kuadrado.svg`,
                                },
                                {
                                    tag: "img",
                                    class: "logo-text",
                                    alt: "Kuadrado",
                                    src: `${images_url}/logo_kuadrado_txt.svg`,
                                },
                            ],
                        },
                        { tag: "h1", contents: "Kuadrado Software", class: "page-contents-center" },
                        {
                            tag: "p",
                            class: "page-contents-center",
                            contents: t("kuadrado-home-description"),
                        },
                        {
                            tag: "ul",
                            class: "philo-bubbles",
                            contents: [t("Simplicité"), t("Légèreté"), t("Écologie")].map(word => {
                                return {
                                    tag: "li",
                                    contents: [{ tag: "span", contents: word }],
                                };
                            }),
                        },
                    ],
                },
                {
                    tag: "section",
                    class: "page-contents-center poles",
                    contents: [
                        {
                            title: t("Jeux"),
                            img: images_url + "/game_controller.svg",
                            href: "/games/",
                            description:
                                t("games-description"),
                        },
                        {
                            title: "Software",
                            img: images_url + "/meca_proc.svg",
                            href: "/software-development/",
                            description: t("software-description"),
                        },
                        // {
                        //     title: "Service",
                        //     img: images_url + "/toolcase.svg",
                        //     href: "/service/",
                        //     description: t("service-description"),
                        // },
                        {
                            title: t("Pédagogie"),
                            img: images_url + "/brain.svg",
                            href: "/education/",
                            description: t("education-description"),
                        },
                    ].map(cardProps => new ThemeCard(cardProps).render()),
                },
            ],
        };
    }
}

module.exports = HomePage;
