"use strict";

const { fetch_json_or_error_text } = require("./fetch");

function getArticleBody(text) {
    return text.replaceAll("\n", "<br/>");
}

function getArticleDate(date) {
    return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
}

function loadArticles(category, locale) {
    return fetch_json_or_error_text(`/articles/${category}/${locale}`);
}

function sortArticles(articles, byField, mode = "desc") {
    return articles.sort((a, b) => mode === "desc" ? a[byField] > b[byField] : a[byField] < b[byField])
}

function renderPlaceholders(n) {
    return Array.from({ length: n }).map(() => {
        return { tag: "div", class: "thumb-placeholder" }
    });
}


module.exports = {
    loadArticles,
    getArticleBody,
    getArticleDate,
    sortArticles,
    renderPlaceholders,
};
