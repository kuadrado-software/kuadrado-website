"use strict";

function fetchjson(url) {
    return new Promise((resolve, reject) => {
        fetch(url)
            .then(r => r.json())
            .then(r => resolve(r))
            .catch(e => reject(e));
    });
}

function fetchtext(url) {
    return new Promise((resolve, reject) => {
        fetch(url)
            .then(r => r.text())
            .then(r => resolve(r))
            .catch(e => reject(e));
    });
}

async function fetch_json_or_error_text(url, options = {}) {
    return new Promise((resolve, reject) => {
        fetch(url, options).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                reject(await res.text());
            } else {
                resolve(await res.json());
            }
        })
    })
}

module.exports = {
    fetchjson,
    fetchtext,
    fetch_json_or_error_text,
};
