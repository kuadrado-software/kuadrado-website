"use strict";
const translator = require("ks-cheap-translator");
const { translations_url } = require("../../constants");

class WebPage {
    constructor(args) {
        Object.assign(this, args);

        if (!this.id) {
            this.id = "webpage-" + performance.now();
        }

        this.translations_ready = false;

        window.t = translator.trad.bind(translator);
        window.translator = translator;

        translator.init({
            translations_url,
            supported_languages: ["fr", "en"],
        }).then(this.refresh_all.bind(this));
    }

    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
    }

    refresh_all() {
        this.translations_ready = true;
        obj2htm.renderCycle();
    }
}

module.exports = WebPage;