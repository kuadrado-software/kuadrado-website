"use strict";

const renderer = require("object-to-html-renderer");
const Template = require("./template/template");


module.exports = function runPage(PageComponent) {
    document.querySelectorAll(".placeholder").forEach(e => e.remove())
    renderer.register("obj2htm");
    const template = new Template({ page: new PageComponent() });
    obj2htm.setRenderCycleRoot(template);
    obj2htm.renderCycle();
};
