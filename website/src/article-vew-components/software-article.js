"use strict";

const ImageCarousel = require("../generic-components/image-carousel");
const { getArticleBody } = require("../lib/article-utils");


class SoftwareArticle {
    constructor(data) {
        this.data = data;
        this.body = getArticleBody(data.body);
    }

    render() {
        const {
            images,
            title,
            subtitle,
            details,
        } = this.data;

        const trad_ready = Object.keys(translator.translations).length > 0;

        const logo = images[0];
        const screens = [...images];
        screens.shift();

        return {
            tag: "article",
            class: "software-article page-contents-center",
            contents: [
                {
                    tag: "div",
                    class: "article-image",
                    contents: [
                        {
                            tag: "img",
                            src: logo
                        },
                    ]
                },
                {
                    tag: "div",
                    class: "article-titles",
                    contents: [
                        {
                            tag: "h1",
                            contents: title,
                            class: "article-title",
                        },
                        {
                            tag: "h2",
                            contents: subtitle,
                            class: "article-subtitle",
                        },
                    ]
                },
                {
                    tag: "div",
                    class: "article-body",
                    contents: [
                        {
                            tag: "div",
                            id: "article-body",
                            contents: [
                                {
                                    tag: "p",
                                    contents: this.body
                                }
                            ],
                        },

                    ]
                },
                {
                    tag: "div",
                    class: "article-more",
                    contents: [
                        trad_ready && screens.length > 0 && new ImageCarousel({ images: screens }).render(),
                        details.length > 0 && {
                            tag: "div",
                            class: "article-details",
                            contents: [
                                {
                                    tag: "h3",
                                    contents: t("Details")
                                },
                                {
                                    tag: "ul",
                                    class: "details-list",
                                    contents: details.map(detail => {
                                        return {
                                            tag: "li",
                                            class: "detail",
                                            contents: [
                                                { tag: "label", contents: detail.label },
                                                {
                                                    tag: "div",
                                                    class: "detail-value",
                                                    contents: detail.value,
                                                },
                                            ],
                                        };
                                    }),
                                },
                            ],
                        },
                    ]
                }

            ],
        };
    }
}

module.exports = SoftwareArticle;
