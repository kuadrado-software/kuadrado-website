"use strict";

const { images_url } = require("../../../constants");

const NAV_MENU_ITEMS = [
    { url: "/games/", text: "Jeux" },

    { url: "/software-development/", text: "Software" },
    // { url: "/service/", text: "Service" },
    {
        url: "/education/",
        text: "Pédagogie",
    }
];

class NavBar {
    constructor() {
        this.initEventHandlers();
    }

    handleBurgerClick() {
        document.getElementById("nav-menu-list").classList.toggle("responsive-show");
    }

    initEventHandlers() {
        window.addEventListener("click", event => {
            if (
                event.target.id !== "nav-menu-list" &&
                !event.target.classList.contains("burger") &&
                !event.target.parentNode.classList.contains("burger")
            ) {
                document.getElementById("nav-menu-list").classList.remove("responsive-show");
            }
        });
    }

    handle_change_lang(lang) {
        const url_path_chunks = location.pathname.split("/");
        const location_contains_locale = translator.supported_languages.find(locale => url_path_chunks.includes(locale));
        const callback = location_contains_locale
            ? function () {
                location.pathname = url_path_chunks.map(chunk => chunk === location_contains_locale ? lang : chunk).join("/");
            }
            : obj2htm.renderCycle.bind(obj2htm);
        translator.update_translations(lang).then(callback).catch(err => console.log(err));
    }

    renderHome() {
        return {
            tag: "div",
            class: "home",
            contents: [
                {
                    tag: "a",
                    href: "/",
                    contents: [
                        {
                            tag: "img",
                            alt: "Logo Kuadrado",
                            src: `${images_url}/logo_kuadrado.svg`,
                        },
                        {
                            tag: "img",
                            alt: "Kuadrado Software",
                            class: "logo-text",
                            src: `${images_url}/logo_kuadrado_txt.svg`,
                        },
                    ],
                },
            ],
        };
    }

    renderMenu(menuItemsArray, isSubmenu = false, parentUrl = "") {
        return {
            tag: "ul",
            id: "nav-menu-list",
            class: isSubmenu ? "submenu" : "",
            contents: menuItemsArray.map(item => {
                const { url, text, submenu } = item;
                const href = `${parentUrl}${url}`;
                return {
                    tag: "li",
                    class: !isSubmenu && window.location.pathname === href ? "active" : "",
                    contents: [
                        {
                            tag: "a",
                            href,
                            contents: t(text),
                        },
                    ].concat(submenu ? [this.renderMenu(submenu, true, url)] : []),
                };
            }).concat({
                tag: "li",
                class: "lang-flags",
                contents: ["fr", "en"].map(lang => {
                    return {
                        tag: "img", src: `${images_url}/flag-${lang}.svg`,
                        class: translator.locale === lang ? "selected" : "",
                        onclick: this.handle_change_lang.bind(this, lang)
                    }
                })
            }),
        };
    }

    renderResponsiveBurger() {
        return {
            tag: "div",
            class: "burger",
            onclick: this.handleBurgerClick.bind(this),
            contents: [{ tag: "span", contents: "···" }],
        };
    }

    render() {
        return {
            tag: "nav",
            contents: [
                this.renderHome(),
                this.renderResponsiveBurger(),
                this.renderMenu(NAV_MENU_ITEMS),
            ],
        };
    }
}

module.exports = NavBar;
