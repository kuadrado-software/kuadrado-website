"use strict";

const { in_construction } = require("../../config");
const { images_url } = require("../../constants");
const NavBar = require("./components/navbar");

class Template {
    constructor(props) {
        this.props = props;
    }
    render() {
        return {
            tag: "main",
            contents: [
                {
                    tag: "header",
                    contents: [new NavBar().render()],
                },
                in_construction && {
                    tag: "section",
                    class: "warning-banner",
                    contents: [
                        {
                            tag: "strong",
                            class: "page-contents-center",
                            contents: t("Site en construction ..."),
                        },
                    ],
                },
                {
                    tag: "section",
                    id: "page-container",
                    contents: [this.props.page.render()],
                },
                {
                    tag: "footer",
                    contents: [
                        {
                            tag: "div",
                            class: "logo",
                            contents: [
                                {
                                    tag: "img",
                                    alt: `logo Kuadrado`,
                                    src: `${images_url}/logo_kuadrado.svg`,
                                },
                                {
                                    tag: "img",
                                    class: "text-logo",
                                    alt: "Kuadrado Software",
                                    src: `${images_url}/logo_kuadrado_txt.svg`,
                                },
                            ],
                        },
                        {
                            tag: "span",
                            contents: "32 rue Simon Vialet, 07240 Vernoux en Vivarais. Ardèche, France",
                        },
                        {
                            tag: "div",
                            contents: [
                                { tag: "strong", contents: "<blue>Contact : </blue>" },
                                {
                                    tag: "a",
                                    href: "mailto:contact@kuadrado-software.fr",
                                    contents: "contact@kuadrado-software.fr",
                                },
                            ],
                        },
                        {
                            tag: "div",
                            class: "social",
                            contents: [
                                {
                                    tag: "strong",
                                    contents: `<blue>${t("Sur les réseaux")} : </blue>`,
                                },
                                {
                                    tag: "a",
                                    href: "https://linkedin.com/company/kuadrado-software",
                                    target: "_blank",
                                    contents: "in",
                                    title: "Linkedin",
                                },
                                {
                                    tag: "a",
                                    href: "https://mastodon.gamedev.place/@kuadrado_software",
                                    target: "_blank",
                                    contents: "m",
                                    title: "Mastodon",
                                }
                            ],
                        },
                        {
                            tag: "span",
                            contents: `Copyleft ${new Date()
                                .getFullYear()} Kuadrado Software | 
                                ${t("kuadrado-footer-copyleft")}`,
                        },
                        {
                            tag: "div", contents: [
                                { tag: "span", contents: t("Ce site web est") + " " },
                                {
                                    tag: "a", target: "_blank",
                                    style_rules: { fontWeight: "bold" },
                                    href: "https://lab.frogg.it/kuadrado-software/kuadrado-website/-/blob/master/README.md",
                                    contents: "OPEN SOURCE"
                                }
                            ]
                        }
                    ],
                },
            ],
        };
    }
}

module.exports = Template;
