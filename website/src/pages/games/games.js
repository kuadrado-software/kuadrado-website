"use strict";

const { images_url } = require("../../../constants");
const WebPage = require("../../lib/web-page");
const GameArticles = require("./components/game-articles");

class GamesPage extends WebPage {
    render() {
        return {
            tag: "div",
            id: "games-page",
            contents: [
                {
                    tag: "div",
                    class: "page-header logo-left",
                    contents: [
                        {
                            tag: "div",
                            class: "page-contents-center grid-wrapper",
                            contents: [
                                {
                                    tag: "div",
                                    class: "logo",
                                    contents: [
                                        {
                                            tag: "img",
                                            alt: "image game controller",
                                            src: `${images_url}/game_controller.svg`,
                                        },
                                    ],
                                },
                                {
                                    tag: "div",
                                    class: "title-box",
                                    contents: [{ tag: "h1", contents: t("Jeux") },]
                                },
                                {
                                    tag: "p",
                                    contents: t("games-page-intro"),
                                },
                            ],
                        },
                    ],
                },
                new GameArticles({ translations_ready: this.translations_ready }).render(),
            ],
        };
    }
}

module.exports = GamesPage;
