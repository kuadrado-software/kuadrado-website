"use strict";

const {
    loadArticles,
    sortArticles,
    renderPlaceholders
} = require("../../../lib/article-utils");
const GameThumb = require("./game-thumb");

class GameArticles {
    constructor(props) {
        this.props = props;
        this.state = {
            articles: [],
        };
        this.id = "game-articles-section";
        this.props.translations_ready && this.loadArticles();
    }

    loadArticles() {
        loadArticles("games", translator.locale)
            .then(articles => {
                this.state.articles = sortArticles(articles, "display_priority_index");
                this.refresh();
            })
            .catch(e => console.log(e));
    }

    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), {
            mode: "replace",
        });
    }

    render() {
        const { articles } = this.state;
        return {
            tag: "section",
            class: "game-articles page-contents-center",
            id: this.id,
            contents:
                articles.length > 0
                    ? articles.map(article => new GameThumb({ ...article }).render())
                    : renderPlaceholders(4),
        };
    }
}

module.exports = GameArticles;
