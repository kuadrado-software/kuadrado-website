"use strict";


class GameThumb {
    constructor(props) {
        this.props = props;
    }

    render() {
        const {
            title,
            subtitle,
            images,
            metadata,
        } = this.props;

        return {
            tag: metadata.view_uri ? "a" : "div",
            href: metadata.view_uri,
            target: "_blank",
            class: "game-thumb",
            style_rules: {
                backgroundImage: `url(${images[0]})`,
            },
            contents: [
                {
                    tag: "div",
                    class: "text-card",
                    contents: [
                        {
                            tag: "strong",
                            class: "game-title",
                            contents: title,
                        },
                        {
                            tag: "strong",
                            class: "game-subtitle",
                            contents: subtitle,
                        },
                        {
                            tag: "span",
                            class: "game-description",
                            contents: metadata.description
                        },
                    ],
                },
            ],
        };
    }
}

module.exports = GameThumb;
