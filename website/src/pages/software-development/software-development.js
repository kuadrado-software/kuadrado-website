"use strict";

const { images_url } = require("../../../constants");
const WebPage = require("../../lib/web-page");
const SoftwareArticles = require("./components/software-articles");

class SoftwareDevelopment extends WebPage {
    render() {
        return {
            tag: "div",
            id: "software-page",
            contents: [
                {
                    tag: "div",
                    class: "page-header logo-left",
                    contents: [
                        {
                            tag: "div",
                            class: "page-contents-center grid-wrapper",
                            contents: [
                                {
                                    tag: "div",
                                    class: "logo",
                                    contents: [
                                        {
                                            tag: "img",
                                            alt: `image mechanic electronic`,
                                            src: `${images_url}/meca_proc.svg`,
                                        },
                                    ],
                                },
                                {
                                    tag: "div",
                                    class: "title-box",
                                    contents: [{ tag: "h1", contents: "Software" }]
                                },
                                {
                                    tag: "p",
                                    contents: t("software-page-intro"),
                                },
                            ],
                        },
                    ],
                },
                new SoftwareArticles().render(),
            ],
        };
    }
}

module.exports = SoftwareDevelopment;
