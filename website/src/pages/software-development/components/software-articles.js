"use strict";

const { loadArticles, sortArticles, renderPlaceholders } = require("../../../lib/article-utils");
const SoftwareThumb = require("./software-thumb");

class SoftwareArticles {
    constructor(props) {
        this.props = props;
        this.state = {
            articles: [],
        };
        this.id = "software-articles-section";
        this.loadArticles();
    }

    loadArticles() {
        loadArticles("software-development", translator.locale)
            .then(articles => {
                this.state.articles = sortArticles(articles, "display_priority_index");;
                this.refresh();
            }).catch(e => console.log(e))
    }
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), {
            mode: "replace",
        });
    }

    render() {
        const { articles } = this.state;
        return {
            tag: "section",
            class: "software-articles page-contents-center",
            id: this.id,
            contents: articles.length > 0
                ? articles.map(article => new SoftwareThumb({ ...article }).render())
                : renderPlaceholders(4),
        };
    }
}

module.exports = SoftwareArticles;
