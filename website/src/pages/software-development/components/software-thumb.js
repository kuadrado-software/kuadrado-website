"use strict";


class SoftwareThumb {
    constructor(props) {
        this.props = props;
    }

    render() {
        const {
            title,
            subtitle,
            images,
            metadata,
        } = this.props;

        return {
            tag: metadata.view_uri ? "a" : "div",
            href: metadata.view_uri,
            target: "_blank",
            class: "software-thumb",
            contents: [
                {
                    tag: "div",
                    class: "software-image",
                    contents: [
                        {
                            tag: "img", src: `${images[0]}`
                        }
                    ]
                },
                {
                    tag: "strong",
                    class: "software-title",
                    contents: title,
                },
                {
                    tag: "span",
                    class: "software-subtitle",
                    contents: subtitle,
                }
            ],
        };
    }
}

module.exports = SoftwareThumb;