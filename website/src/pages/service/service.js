const { images_url } = require("../../../constants");
const WebPage = require("../../lib/web-page");

class ServicePage extends WebPage {
    render() {
        return {
            tag: "div",
            id: "service-page",
            contents: [
                {
                    tag: "div",
                    class: "page-header logo-left",
                    contents: [
                        {
                            tag: "div",
                            class: "page-contents-center grid-wrapper",
                            contents: [
                                {
                                    tag: "div",
                                    class: "logo",
                                    contents: [
                                        {
                                            tag: "img",
                                            alt: "An open tool case",
                                            src: `${images_url}/toolcase.svg`,
                                        },
                                    ],
                                },
                                {
                                    tag: "div",
                                    class: "title-box",
                                    contents: [{ tag: "h1", contents: "Service" }]
                                },
                                {
                                    tag: "p",
                                    contents: t("service-page-intro"),
                                },
                            ],
                        },
                    ],
                },
                {
                    tag: "div",
                    id: "service-sections",
                    class: "page-contents-center",
                    contents: [
                        {
                            tag: "section",
                            class: "img-left",
                            contents: [
                                {
                                    tag: "img",
                                    width: 300,
                                    height: 300,
                                    src: `${images_url}/internet_earth.png`,
                                    alt: "Planet Earth with electrons"
                                },
                                {
                                    tag: "div",
                                    class: "section-data",
                                    contents: [
                                        { tag: "h3", contents: "Web" },
                                        { tag: "p", contents: t("Sites web légers et rapides avec outil de gestion de contenu incorporé.") },
                                        {
                                            tag: "ul", contents: [
                                                t("Gestion de l'hébergement"),
                                                t("Maintenance et évolution"),
                                                t("Référencement sur les moteurs de recherche"),
                                            ].map(li_txt => {
                                                return {
                                                    tag: "li",
                                                    contents: li_txt
                                                }
                                            })
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            tag: "section",
                            class: "img-right",
                            contents: [
                                { tag: "img", width: 300, height: 300, src: `${images_url}/meca_proc.svg` },
                                {
                                    tag: "div",
                                    class: "section-data",
                                    contents: [
                                        { tag: "h3", contents: t("Logiciels professionnels - Applications métier") },
                                        { tag: "p", contents: t("Optimisez la gestion de votre activité avec un logiciel adapté ou proposez une interface numérique à votre public.") },
                                    ]
                                }
                            ]
                        },
                        {
                            tag: "section",
                            class: "img-left",
                            contents: [
                                { tag: "img", width: 300, height: 300, src: `${images_url}/workcycle.svg` },
                                {
                                    tag: "div",
                                    class: "section-data",
                                    contents: [
                                        { tag: "p", contents: t("Kuadrado Software vous accompagne sur votre projet de A à Z") },
                                        {
                                            tag: "ul", contents: [
                                                t("Identification du métier et du besoin"),
                                                t("Définition d'un cahier des charges adapté à vos moyens"),
                                                t("Développement itératif") + "<br /><em>&emsp;&emsp;(" + t("nous validons ensemble chaque étape du développement") + ")<em/>",
                                                t("Maintenance, mises à jours, déploiement, hébergement ..."),
                                            ].map(li_txt => {
                                                return {
                                                    tag: "li",
                                                    contents: li_txt
                                                }
                                            })
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            tag: "section",
                            contents: [
                                {
                                    tag: "a",
                                    href: "mailto:contact@kuadrado-software.fr",
                                    class: "contact-button",
                                    contents: [
                                        { tag: "span", class: "mail-icon", contents: "📧" },
                                        { tag: "span", class: "btn-text", contents: t("Demander un devis") }
                                    ]
                                },
                            ]
                        },
                        { tag: "hr" },
                        {
                            tag: "section",
                            class: "techno",
                            contents: [
                                { tag: "h3", contents: t("Technologie") },
                                {
                                    tag: "ul", contents: [
                                        [
                                            "rust_logo.svg",
                                            "Rust",
                                            t("rust-description")
                                        ],
                                        [
                                            "mongodb_logo.svg",
                                            "MongoDB",
                                            t("Bases de données flexibles et scalables.")
                                        ],
                                        [
                                            "js_logo.svg",
                                            "JavaScript",
                                            t("js-description")
                                        ],
                                        [
                                            "docker_logo.svg",
                                            "Docker",
                                            t("docker-description")
                                        ]
                                    ].map(li => {
                                        const [img_name, title, txt] = li;
                                        return {
                                            tag: "li",
                                            contents: [
                                                {
                                                    tag: "img",
                                                    width: 100,
                                                    height: 100,
                                                    src: `${images_url}/${img_name}`,
                                                },
                                                {
                                                    tag: "div",
                                                    class: "tech-txt",
                                                    contents: [
                                                        { tag: "h4", contents: title },
                                                        {
                                                            tag: "p",
                                                            contents: txt,
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    })
                                }
                            ]
                        },
                        { tag: "hr" },
                        {
                            tag: "section",
                            contents: [
                                {
                                    tag: "p",
                                    style_rules: { margin: 0, fontSize: "18px" },
                                    contents: t("Pour un devis, un renseignement, ou un premier échange autour de votre projet")
                                },
                                {
                                    tag: "a",
                                    href: "mailto:contact@kuadrado-software.fr",
                                    class: "contact-button",
                                    contents: [
                                        { tag: "span", class: "mail-icon", contents: "📧" },
                                        { tag: "span", class: "btn-text", contents: "contact@kuadrado-software.fr" }
                                    ]
                                },
                            ]
                        },
                    ]
                }
            ]

        }
    }
}

module.exports = ServicePage;