"use strict";

const { images_url } = require("../../../constants");
const WebPage = require("../../lib/web-page");
const EduArticles = require("./components/edu-articles");

const EDU_THEMES = [
    {
        title: "Programmation",
        description: "edu-learn-coding",
        image: "learning_theme_coding.png",
    },
    {
        title: "Dessin numérique et animation 2D",
        description: "edu-learn-2d",
        image: "learning_theme_2d.png",
    },
    {
        title: "Maths et physique",
        description: "edu-learn-math",
        image: "learning_theme_math.png",
    },
    {
        title: "Aide informatique générale",
        description: "edu-learn-computer",
        image: "learning_theme_pc.png",
    },
    {
        title: "Stage GNU/Linux",
        description: "edu-learn-gnu",
        image: "learning_theme_linux.png"
    },
    {
        title: "Créer un jeu avec Mentalo",
        description: "edu-learn-mentalo",
        image: "learning_theme_mentalo.png",
    }
];

class EducationPage extends WebPage {
    render() {
        return {
            tag: "div",
            id: "education-page",
            typeof: "EducationalOrganization",
            contents: [
                {
                    tag: "div",
                    class: "page-header logo-left",
                    contents: [
                        {
                            tag: "div",
                            class: "page-contents-center grid-wrapper",
                            contents: [
                                {
                                    tag: "div",
                                    class: "logo",
                                    contents: [
                                        {
                                            tag: "img",
                                            alt: "image brain",
                                            src: `${images_url}/brain.svg`,
                                        },
                                    ],
                                },
                                {
                                    tag: "div",
                                    class: "title-box",
                                    contents: [{ tag: "h1", contents: t("Pédagogie") },]
                                },
                                {
                                    tag: "p",
                                    contents: t("edu-page-intro"),
                                },
                            ],
                        },
                    ],
                },
                {
                    tag: "div",
                    class: "title-banner",
                },
                {
                    tag: "section",
                    contents: [
                        {
                            tag: "div",
                            class: "page-contents-center",
                            contents: [
                                {
                                    tag: "ul",
                                    class: "edu-themes",
                                    contents: EDU_THEMES.map(theme => {
                                        return {
                                            tag: "li",
                                            class: "edu-theme",
                                            contents: [
                                                { tag: "img", width: 250, height: 140, class: "pixelated", src: `${images_url}/${theme.image}` },
                                                { tag: "h3", contents: t(theme.title) },
                                                { tag: "p", contents: t(theme.description) },
                                            ]
                                        }
                                    })
                                },
                            ]
                        },
                    ]
                },
                { tag: "h2", class: "edu-section-title page-contents-center", contents: t("Programme XXXX", { date: "2022" }) },
                new EduArticles().render(),
            ],
        };
    }
}

module.exports = EducationPage;
