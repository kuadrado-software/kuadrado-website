"use strict";
const { loadArticles, sortArticles, renderPlaceholders } = require("../../../lib/article-utils");
const EduArticle = require("./edu-article");

class EduArticles {
    constructor(props) {
        this.props = props;
        this.state = {
            articles: [],
            loaded: false,
        };
        this.id = "edu-articles-section";
        this.loadArticles();
    }

    loadArticles() {
        loadArticles("education", translator.locale)
            .then(articles => {
                this.state.articles = sortArticles(articles, "display_priority_index");
            })
            .catch(e => console.log(e))
            .finally(() => {
                this.state.loaded = true;
                this.refresh()
            });
    }

    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), {
            mode: "replace",
        });
    }

    render() {
        const { articles, loaded } = this.state;
        return {
            tag: "section",
            class: "edu-articles page-contents-center",
            id: this.id,
            contents:
                loaded && articles.length > 0
                    ? articles.map(article => new EduArticle({ ...article }).render())
                    : loaded && articles.length === 0 ? [
                        { tag: "p", contents: t("Rien de prévu pour le moment") }
                    ] : renderPlaceholders(2),
        };
    }
}

module.exports = EduArticles;