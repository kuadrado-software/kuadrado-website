"use strict";

const ImageCarousel = require("../../../generic-components/image-carousel");
const { getArticleBody } = require("../../../lib/article-utils");

class EduArticle {
    constructor(props) {
        this.props = props;
    }

    render() {
        const { title, body, subtitle, images, details = [] } = this.props;

        return {
            tag: "article",
            class: "edu-article",
            typeof: "Article",
            contents: [
                {
                    tag: "h2",
                    class: "edu-art-title",
                    contents: title,
                    property: "name",
                },
                {
                    tag: "div", class: "edu-art-image",
                    contents: [
                        {
                            tag: "img", src: images[0]
                        }
                    ]
                },
                {
                    tag: "h3",
                    class: "edu-art-subtitle",
                    contents: subtitle,
                    property: "alternativeHeadline",
                },
                {
                    tag: "div",
                    class: "edu-art-description",
                    contents: getArticleBody(body),
                    property: "description",
                },
                images.length > 1 && {
                    tag: "div", class: "edu-art-carousel", contents: [
                        new ImageCarousel({ images: images }).render()
                    ]
                },
                details.length > 0 && {
                    tag: "div",
                    class: "article-details edu-art-details",
                    contents: [
                        {
                            tag: "h2",
                            contents: "Details",
                        },
                        {
                            tag: "ul",
                            class: "details-list",
                            contents: details.map(detail => {
                                return {
                                    tag: "li",
                                    class: "detail",
                                    contents: [
                                        { tag: "label", contents: detail.label },
                                        {
                                            tag: "div",
                                            class: "detail-value",
                                            contents: detail.value
                                        },
                                    ],
                                };
                            }),
                        },
                    ],
                },
            ],
        };
    }
}

module.exports = EduArticle;