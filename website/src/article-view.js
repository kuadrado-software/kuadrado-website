"use strict";

const GameArticle = require("./article-vew-components/game-article");
const SoftwareArticle = require("./article-vew-components/software-article");
const WebPage = require("./lib/web-page");
const runPage = require("./run-page");

class ArticlePage extends WebPage {
    constructor() {
        super({ id: "article-page" });
        this.article = JSON.parse(document.getElementById("article-json").innerHTML);
    }

    render_article() {
        switch (this.article.category) {
            case "games":
                return new GameArticle(this.article).render();
            // case "education":
            //     return;
            case "software-development":
                return new SoftwareArticle(this.article).render();
        }
    }

    render() {
        return {
            tag: "div",
            id: "article-view-container",
            contents: [
                this.render_article(),
            ],
        }
    }
}

runPage(ArticlePage);
