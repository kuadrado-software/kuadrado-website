"use strict";

class ThemeCard {
    constructor(props) {
        this.props = props;
    }

    render() {
        return {
            tag: "a",
            class: "theme-card",
            href: this.props.href,
            contents: [
                {
                    tag: "div",
                    class: "card-img",
                    contents: [
                        {
                            tag: "img",
                            alt: `thematic image ${this.props.img.split("/").reverse()[0].replace(/\.[A-Za-z]+/, "")}`,
                            src: this.props.img
                        }],
                },
                {
                    tag: "div",
                    class: "card-title",
                    contents: [{ tag: "h2", class: "section-title", contents: this.props.title }],
                },
                {
                    tag: "div",
                    class: "card-description",
                    contents: [{ tag: "p", contents: this.props.description }],
                },
            ],
        };
    }
}

module.exports = ThemeCard;
