# Kuadrado Software Website

## install

### Prerequisite

-   docker
-   docker-compose
-   certbot for prod or mkcert for dev

---

### TLS CERTS

-   ### Localhost

    With mkcert

    ```sh
    $ sudo mkdir -p /etc/letsencrypt/live/localhost
    $ sudo chmod 777 -R /etc/letsencryt/live/localhost
    $ mkcert -key-file privkey.pem -cert-file fullchain.pem localhost
    ```

-   ### Real server

    Install snapd and certbot at system level. https://certbot.eff.org/lets-encrypt/debianbuster-other

    Then run

    ```sh
    $ sudo certbot certonly --standalone
    ```

---

## Build

```sh
$ git clone https://gitlab.com/kuadrado-software/kuadrado-website.git
$ cd kuadrado-website

$ touch ./.env
# Fill .env file with the required variables described below

# Build the website
$ cd website
$ npm install
$ npm run build
# or
$ npm run build-debug

$ cd ..

# build the image and run the container
$ docker-compose up --build
# Or
$ docker-compose -f ./dev.docker-compose.yml up --build
```

---

## Expected environment variables

_Env vars may be defined in a .env file at the root of the project_

-   `RELEASE_MODE`: Can be either "debug", "test" or "prod". It's used to define the log level. Not required. Default is "prod"
-   `SERVER_PORT`: The port bound the Actix server
-   `SERVER_PORT_TLS`: The tls port bound the Actix server
-   `SERVER_HOST`: IP address or domain name hosting the API.
-   `SERVER_PROTOCOL`: http or https regarding the protocol used to connect the server
-   `RESOURCES_DIR`: A linux directory that will be used to mount static resources like certs and html files
-   `DATABASE_NAME`: The name of the mongodb database
-   `DB_PORT`: The port of the database (MondbDB default is 27017)
-   `DB_ROOT_USERNAME`: Database root user name
-   `DB_ROOT_PASSWORD`: Database root password
-   `DB_USERNAME`: Database regular user name
-   `DB_USER_PASSWORD`: Database regular user password
-   `DEFAULT_ADMIN_USERNAME`: Default website administrator username (admin panel authentication)
-   `DEFAULT_ADMIN_PASSWORD`: Default website administrator password (admin panel authentication)
-   `CRYPT_KEY`: Any random ascii string that will be used to encrypt data like emails and passwords.


## Tests
Create a public/standard/test_sitemap.xml file with a sitemap structure in order to pass the tests.
Create a public/testing empty directory

## Admin panel - create a Play Button
**Syntax**

In the article body
```json
[PLAY_BUTTON {"engine": <engine-name>, "filename": <game-file-name>}]
```
Supported engine at the moment: `"mentalo"`