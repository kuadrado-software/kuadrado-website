const renderer = require("object-to-html-renderer");
const RootComponent = require("./components/root");

renderer.register("obj2htm");
obj2htm.setRenderCycleRoot(new RootComponent());
obj2htm.renderCycle();