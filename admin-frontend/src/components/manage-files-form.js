"use strict";

const { fetch_post_file, fetch_delete_static_file } = require("../xhr");

class FilesIndexView {
    constructor(params) {
        this.params = params;
        this.show_category = Object.keys(this.params.get_files_index())[0];
        this.id = "files-index-view";
    }

    get_files_list_style(category) {
        const base_style = {
            listStyleType: "none",
            margin: 0,
            padding: 0,
            overflow: "auto",
            height: "300px",
        };

        switch (category) {
            case "images":
                return {
                    display: "flex",
                    flexWrap: "wrap",
                    gap: "5px",
                    ...base_style,
                }
            case "sounds":
                return base_style // TMP
            default:
                return base_style
        }
    }

    get_item_view(item) {
        const get_link = () => {
            switch (this.show_category) {
                case "images":
                    return {
                        tag: "a",
                        href: item,
                        target: "_blank",
                        style_rules: {
                            backgroundImage: `url(${item})`,
                            backgroundRepeat: "no-repeat",
                            backgroundSize: "cover",
                            backgroundPosition: "center",
                            width: "100px",
                            height: "100px",
                            display: "block"
                        }
                    }
                case "sounds":
                    return {
                        tag: "div",
                        contents: [
                            {
                                tag: "audio",
                                controls: true,
                                contents: [
                                    {
                                        tag: "source",
                                        src: item
                                    }]
                            },
                            {
                                tag: "a",
                                href: item,
                                target: "_blank",
                                contents: item
                            },
                        ]
                    }
                default:
                    return {
                        tag: "a",
                        href: item,
                        target: "_blank",
                        contents: item
                    }
            }
        }

        return {
            tag: "div",
            contents: [
                get_link(),
                {
                    tag: "button",
                    contents: "DEL",
                    onclick: () => {
                        const rev_split = item.split("/").reverse();
                        const filename = rev_split.shift();
                        const category = rev_split.shift();
                        const dir = rev_split[0] === "uploads" ? "uploads" : "base";
                        fetch_delete_static_file(dir, category, filename).then(res => {
                            this.params.refresh_index(this.refresh.bind(this));
                        }).catch(err => console.log(err))
                    }
                }
            ]
        }
    }

    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" })
    }

    render() {
        const files_index = this.params.get_files_index();
        return {
            tag: "div",
            id: this.id,
            contents: [
                {
                    tag: "ul",
                    style_rules: {
                        display: "flex",
                        listStyleType: "none",
                        margin: 0,
                        padding: 0,
                    },
                    contents: Object.keys(files_index).map(cat => {
                        return {
                            tag: "li",
                            style_rules: {
                                cursor: "pointer",
                                fontWeight: "bold",
                                padding: "20px",
                                border: cat === this.show_category ? "1px solid black" : "none"
                            },
                            contents: cat,
                            onclick: () => {
                                this.show_category = cat;
                                this.refresh();
                            }
                        }
                    })
                },
                {
                    tag: "ul",
                    style_rules: this.get_files_list_style(this.show_category),
                    contents: files_index[this.show_category].map(item => {
                        return {
                            tag: "li",
                            contents: [
                                this.get_item_view(item)
                            ]
                        }
                    }),
                }
            ]
        }
    }
}

class ManageFilesForm {
    constructor(params) {
        this.params = params;
        this.id = "static-file-manager-view";
    }

    render_index_view() {
        return new FilesIndexView(this.params).render()
    }

    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" })
    }

    render() {
        return {
            tag: "div",
            id: this.id,
            contents: [
                this.render_index_view(),
                {
                    tag: "form",
                    style_rules: { border: "1px solid black" },
                    enctype: "multipart/form-data",
                    onsubmit: e => {
                        e.preventDefault();
                        fetch_post_file(e.target).then(() => {
                            this.params.refresh_index(this.refresh.bind(this))
                        }).catch(err => console.log(err));
                    },
                    contents: [
                        { tag: "input", name: "file", type: "file", multiple: true },
                        { tag: "input", type: "submit" }
                    ]
                }
            ]
        }
    }
}

module.exports = ManageFilesForm;