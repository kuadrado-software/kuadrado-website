"use strict";

const { fetch_delete_article, fetch_article } = require("../xhr");
const ArticleList = require("./articles-list");
const CreateArticleForm = require("./create-article-form");

class UpdateArticleForm {
    constructor(params) {
        this.params = params;
        this.state = {
            search_article_title: "",
            article_to_update: {},
        };

        this.articles_list = new ArticleList({
            on_select_article: this.handle_select_article.bind(this),
            on_delete_article: this.handle_delete_article.bind(this)
        });
    }

    reset() {
        this.state = {
            search_article_title: "",
            search_result: {},
            article_to_update: {},
        };
    }

    handle_select_article(article) {
        fetch_article(article._id.$oid)
            .then(art => {
                this.state.article_to_update = art;
                this.refresh_update_form();

            }).catch(err => console.log(err))
    }

    handle_delete_article(article) {
        fetch_delete_article(article._id.$oid)
            .then(res => {
                alert(res);
                this.reset();
                this.refresh();
                this.articles_list.fetch_list(); // bof..
            })
            .catch(err => alert(err))
    }

    refresh_update_form() {
        obj2htm.subRender(
            this.render_update_form(),
            document.getElementById("update-article-form-container"),
            { mode: "replace" },
        );
    }

    render_update_form() {
        return {
            tag: "div",
            id: "update-article-form-container",
            contents: this.state.article_to_update._id
                ? [new CreateArticleForm({
                    data: this.state.article_to_update,
                    on_article_sent: () => {
                        this.reset();
                        this.articles_list.fetch_list();
                    },
                    ...this.params
                }).render()]
                : []
        }
    }

    refresh() {
        obj2htm.subRender(this.render(), document.getElementById("update-article-multiform"), { mode: "replace" })
    }

    render() {
        return {
            tag: "div",
            id: "update-article-multiform",
            style_rules: {
                display: "flex",
                flexDirection: "column",
                gap: "20px",
            },
            contents: [
                this.articles_list.render(),
                { tag: "hr", style_rules: { width: "100%" } },
                this.render_update_form(),
            ]
        }
    }
}

module.exports = UpdateArticleForm;