
const { fetch_static_files_index } = require("../xhr");
const CreateArticleForm = require("./create-article-form");
const ManageFilesForm = require("./manage-files-form");
const UpdateArticleForm = require("./update-article-form");

class RootComponent {
    constructor() {
        this.state = {
            selected_tab: "",
            loading_index: true,
            static_files_index: {}
        };

        this.fetch_files_index(obj2htm.renderCycle.bind(obj2htm));
    }

    build_index_object(files_urls) {
        return files_urls.reduce((o, url) => {
            const split_url = url.split("/");
            const cat = split_url[(split_url[2] === "uploads") ? 3 : 2];
            o[cat] = o[cat] ? [...o[cat], url] : [url];
            return o;
        }, {});
    }

    fetch_files_index(cb) {
        fetch_static_files_index()
            .then(files =>
                this.state.static_files_index = this.build_index_object(files)
            )
            .catch(err => console.log(err))
            .finally(() => {
                this.state.loading_index = false;
                cb();
            });
    }

    handle_nav_click(e) {
        this.state.selected_tab = e.target.tab_name;
        obj2htm.renderCycle();
    }

    render_state() {
        const files_index = this.state.static_files_index;
        switch (this.state.selected_tab) {
            case "create":
                return new CreateArticleForm({ files_index }).render();
            case "update":
                return new UpdateArticleForm({ files_index }).render();
            case "files":
                return new ManageFilesForm({ get_files_index: () => this.state.static_files_index, refresh_index: this.fetch_files_index.bind(this) }).render()
            default:
                return undefined;
        }
    }

    render() {
        return {
            tag: "main",
            contents: [
                { tag: "h1", contents: "Kuadrado admin panel" },
                {
                    tag: "nav",
                    contents: [
                        {
                            tag: "span", contents: "Create article", tab_name: "create",
                            class: this.state.selected_tab === "create" ? "selected" : "",
                            onclick: this.handle_nav_click.bind(this),
                        },
                        {
                            tag: "span", contents: "Update article", tab_name: "update",
                            class: this.state.selected_tab === "update" ? "selected" : "",
                            onclick: this.handle_nav_click.bind(this),
                        },
                        {
                            tag: "span", contents: "Manage files", tab_name: "files",
                            class: this.state.selected_tab === "files" ? "selected" : "",
                            onclick: this.handle_nav_click.bind(this),
                        },
                    ],
                },
                this.render_state(),
            ],
        };
    }
}

module.exports = RootComponent;