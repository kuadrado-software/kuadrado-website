"use strict";

const { fetch_all_articles } = require("../xhr");

class ArticleList {
    constructor(params) {
        this.params = params;
        this.state = {
            articles: []
        }
        this.fetch_list();
    }

    fetch_list() {
        fetch_all_articles()
            .then(articles => {
                this.state.articles = articles.sort((a, b) => a.display_priority_index > b.display_priority_index);
                this.refresh_list();
            })
            .catch(err => console.log(err))
    }

    refresh_list() {
        obj2htm.subRender(this.render_list(), document.getElementById("browse-articles-results"), { mode: "replace" })
    }

    render_list() {
        return {
            tag: "ul",
            id: "browse-articles-results",
            style_rules: {
                maxHeight: "300px",
                overflowY: "scroll",
            },
            contents: this.state.articles.map(art => {
                return {
                    tag: "li",
                    style_rules: {
                        display: "grid",
                        gridTemplateColumns: "auto auto 100px 100px",
                        gap: "10px",
                        alignItems: "center"
                    },
                    contents: [
                        { tag: "span", contents: `[${art.locale}] [${art.category}] <b>${art.title}</b> - ${art._id.$oid}` },
                        art.with_static_view ? {
                            tag: "a",
                            href: art.metadata.view_uri,
                            contents: art.metadata.view_uri,
                            target: "_blank"
                        } : { tag: "span" },
                        {
                            tag: "button", contents: "Select", onclick: () => {
                                this.params.on_select_article(art)
                            }
                        },
                        {
                            tag: "button", contents: "Delete", onclick: () => {
                                this.params.on_delete_article(art)
                            }
                        },
                    ]
                };
            }),
        }
    }

    render() {
        return {
            tag: "div",
            contents: [
                { tag: "button", onclick: this.fetch_list.bind(this), contents: "REFRESH" },
                this.render_list()
            ],
        }
    }
}

module.exports = ArticleList;