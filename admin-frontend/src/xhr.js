function fetch_article(article_id) {
    return new Promise((resolve, reject) => {
        fetch(`/article/${article_id}`).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                resolve(await res.json());
            }
        }).catch(e => reject(e))
    })
}

function fetch_articles_by_category(category) {
    return new Promise((resolve, reject) => {
        fetch(`/articles/${category}`).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                resolve(await res.json());
            }
        }).catch(e => reject(e))
    })
}

function fetch_post_article(article_data) {
    return new Promise((resolve, reject) => {
        fetch("/post-article", {
            credentials: 'include',
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(article_data),
        })
            .then(async res => {
                if (res.status >= 400 && res.status < 600) {
                    const text = await res.text();
                    reject(text)
                } else {
                    const json = await res.json();
                    resolve(json);
                }
            })
            .catch(err => reject(err))
    })
}

function fetch_update_article(article_data) {
    return new Promise((resolve, reject) => {
        fetch(`/update-article/${article_data._id.$oid}`, {
            credentials: 'include',
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(article_data),
        })
            .then(async res => {
                if (res.status >= 400 && res.status < 600) {
                    const text = await res.text();
                    reject(text)
                } else {
                    const json = await res.json();
                    resolve(json);
                }
            })
            .catch(err => reject(err))
    })
}

function fetch_delete_article(article_id) {
    return new Promise((resolve, reject) => {
        fetch(`/delete-article/${article_id}`, {
            credentials: 'include',
            method: "DELETE"
        })
            .then(async res => {
                const text = await res.text();
                if (res.status >= 400 && res.status < 600) {
                    reject(text)
                } else {
                    resolve(text);
                }
            })
            .catch(err => reject(err))
    });
}

function fetch_all_articles() {
    return new Promise((resolve, reject) => {
        fetch(`/articles`)
            .then(async res => {
                if (res.status >= 400 && res.status < 600) {
                    const text = await res.text();
                    reject(text)
                } else {
                    const json = await res.json();
                    resolve(json);
                }
            })
            .catch(err => reject(err))
    });
}

function fetch_post_file(form) {
    return new Promise((resolve, reject) => {
        fetch("/post-files", {
            method: "POST",
            body: new FormData(form),
        }).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                reject((await res.text()))
            } else {
                resolve((await res.json()));
            }
        }).catch(err => reject(err))
    })
}

function fetch_static_files_index() {
    return new Promise((resolve, reject) => {
        fetch("/static-files-index").then(async res => {
            if (res.status >= 400 && res.status < 600) {
                reject((await res.text()))
            } else {
                resolve((await res.json()));
            }
        }).catch(err => reject(err))
    })
}

function fetch_delete_static_file(dir, category, filename) {
    return new Promise((resolve, reject) => {
        fetch(`/delete-file/${dir}/${category}/${filename}`, {
            credentials: 'include',
            method: "DELETE",
        }).then(async res => {
            const text = await res.text();
            if (res.status >= 400 && res.status < 600) {
                reject(text)
            } else {
                resolve(text);
            }
        }).catch(err => reject(err))
    })
}


module.exports = {
    fetch_article,
    fetch_articles_by_category,
    fetch_post_article,
    fetch_update_article,
    fetch_delete_article,
    fetch_all_articles,
    fetch_post_file,
    fetch_static_files_index,
    fetch_delete_static_file,
}