function get_text_date(iso_str) {
    const date = new Date(iso_str);
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} - ${date.getHours()}h${date.getMinutes()}mn`
}

module.exports = {
    get_text_date
}