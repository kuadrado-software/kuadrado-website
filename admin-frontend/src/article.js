"use strict";

class Article {
    constructor(data) {
        if (data) {
            this.from(data)
        } else {
            this.title = "";
            this.subtitle = "";
            this.category = "";
            this.details = [];
            this.images = [];
            this.body = "";
            this.locale = "";
            this.display_priority_index = 1;
            this.with_static_view = true;
            this.metadata = {
                description: "",
            };
        }
    }

    from(data) {
        Object.entries(data).forEach(k_v => {
            const [key, value] = k_v;
            this[key] = value;
        });
    }
}
module.exports = Article;