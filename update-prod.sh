#!/bin/sh

git checkout .
git pull froggit master

cd admin-frontend
npm install
npm run build

cd ../website
npm install
npm run build

cd ..
docker compose up --build
