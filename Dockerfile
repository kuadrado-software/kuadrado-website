FROM kuadsoft/rust-openssl:bullseye-slim as builder

WORKDIR /usr/src/kuadrado_server
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
COPY ./src ./src
RUN cargo install --locked --path .
 
FROM kuadsoft/debian-openssl:bullseye-slim
COPY --from=builder /usr/local/cargo/bin/kuadrado_server /usr/local/bin/kuadrado_server
CMD ["kuadrado_server"]